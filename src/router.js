import Vue from "vue";
import Router from "vue-router";
import Home from "./pages/Home/Home";

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: "/", name: "home", component: Home },
    {
      path: "/resume",
      name: "resume",
      component: () => import("./pages/Resume/Resume")
    }
  ]
});

export default router;

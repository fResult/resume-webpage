export default {
  data() {
    return {
      instituteName: "",
      instituteProvince: "",
      instituteDegree: ""
    };
  },
  methods: {
    setName(name) {
      this.instituteName = name
    },
    getName() {
      return this.instituteName
    }
  }
};
